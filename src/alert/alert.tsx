import { Close } from '../close';
import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IAlertProps extends IBaseElementComponent {
  closable?: boolean;
  styleType?: 'primary' | 'success' | 'warning' | 'danger';
}

@observer
export class Alert extends ElementComponent<IAlertProps, any> {
  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {
        this.props.closable ?
          <Close class={'uk-alert-close'} /> :
          null
      }
      {this.props.children}
    </div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style
      this.props.styleType ? `uk-alert-${this.props.styleType}` : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ['uk-alert']: true,
      ...super.attributes,
    };
  }
}
