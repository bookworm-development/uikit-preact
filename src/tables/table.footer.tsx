import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Footer extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = '';

  public render() {
    return <tfoot className={this.className} {...this.attributes}>
      {this.props.children}
    </tfoot>;
  }
}
