import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

interface ITableRowProps extends IBaseElementComponent {
  align?: 'middle';
}

@observer
export class Row extends ElementComponent<ITableRowProps, any> {
  protected baseClass = '';

  public render() {
    return <tr className={this.className} {...this.attributes}>
      {this.props.children}
    </tr>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Align
      this.props.align ? `uk-table-${this.props.align}` : '',
    ]);
  }
}
