import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Item } from './lightbox.item';

export interface ILightBoxProps extends IBaseElementComponent {
  options?: IModalOptions;
}

interface IModalOptions {
  ['autoplay-interval']: number;
  ['delay-controls']: number;
  ['pause-on-hover']: boolean;
  ['video-autoplay']: boolean;
  animation: string;
  autoplay: boolean;
  index: number;
  preload: number;
  template: string;
  velocity: number;
}

@observer
export class LightBox extends ElementComponent<ILightBoxProps, any> {
  public static Item = Item;

  protected baseClass = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // LightBox
      ['uk-lightbox']: this.toAttributeValue(
        {
          ['autoplay-interval']: 7000,
          ['delay-controls']: 3000,
          ['pause-on-hover']: false,
          ['video-autoplay']: false,
          animation: 'slide',
          autoplay: false,
          index: 0,
          preload: 1,
          template: '',
          velocity: 2,
        },
        this.props.options || {},
      ),
    };
  }
}
