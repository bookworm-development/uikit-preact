import { h, render } from 'preact';
import * as UIKit from 'uikit';

import { NotificationsGroup } from './notification.group';

interface INotificationUIKitInstance {
  $el: HTMLElement;
  close: (immediate?: boolean) => void;
}

export class Notification {
  private instance: INotificationUIKitInstance | null;
  private group: NotificationsGroup;

  public constructor(options: INotificationOptions, group: NotificationsGroup) {
    this.group = group;

    this.instance = UIKit.notification({
      ...options,
      message: render(<span>{options.message}</span>, document.createDocumentFragment()).innerHTML,
    });

    UIKit.util.on((this.instance as INotificationUIKitInstance).$el, 'close', () => {
      // Closed due to timeout
      this.instance = null;

      this.group.close(this);
    });
  }

  public close(immediate?: boolean) {
    if (this.instance) {
      this.instance.close(!!immediate);
      this.instance = null;

      // Notify the group of a notification close
      this.group.close(this);

      return true;
    }

    return false;
  }

  public get closed() {
    return !this.instance;
  }
}

export interface INotificationOptions {
  message: JSX.Element | JSX.Element[] | string;
  status?: NotificationStatus;
  timeout?: number;
  group?: string;
  pos?: NotificationPosition;
}

type NotificationStatus = 'primary' | 'success' | 'warning' | 'danger';

type NotificationPosition =
  'top-left' | 'top-center' | 'top-right' |
  'bottom-left' | 'bottom-center' | 'bottom-right';
