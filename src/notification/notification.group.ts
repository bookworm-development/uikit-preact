import { INotificationOptions, Notification } from './notification';

export class NotificationsGroup {
  private notifications: Notification[];
  private gn: string;

  public constructor(groupName: string) {
    this.gn = groupName;
    this.notifications = [];
  }

  // Group name getter
  public get groupName() {
    return this.gn;
  }

  // Create a new notification and returns the instance
  public new(options: INotificationOptions) {
    const n = new Notification({ ...options, group: this.gn }, this);
    this.notifications.push(n);

    return n;
  }

  // Close all notifications in the group
  public closeAll(immediate: boolean = false) {
    this.notifications = this.notifications.filter((n) => {
      return !n.close(immediate);
    });
  }

  // Given a notification instance closes it if it's part of the current group
  public close(n: Notification) {
    this.notifications = this.notifications.filter((nn) => {
      if (nn === n) {
        nn.close();

        return false;
      }

      return true;
    });
  }
}
