import { computed, ElementComponent, h, IBaseElementComponent, ISwitcherOptions, observer } from '../core';
import { Item, TabItem } from './item';

export interface ITabProps extends IBaseElementComponent {
  options?: ITabOptions;
  items: TabItem[];
  direction?: 'left' | 'right' | 'bottom';
}

interface ITabOptions extends ISwitcherOptions {
  media?: string | number;
}

@observer
export class Tab extends ElementComponent<ITabProps, any> {
  protected baseClass = '';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      {
        this.props.items.map((i) => <Item {...i} />)
      }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Direction
      this.props.direction ? `uk-tab-${this.props.direction}` : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      ['uk-tab']: this.toAttributeValue(
        {
          active: 0,
          animation: false,
          connect: false,
          duration: 200,
          media: 960,
          swiping: true,
          toggle: '> *',
        },
        this.props.options || {},
      ),
    };
  }
}
