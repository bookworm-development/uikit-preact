import { computed, ElementChild, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Link } from '../link';

interface ITabItem {
  label: ElementChild;
  disabled?: boolean;
  dropdown?: JSX.Element;
}

export type TabItem = ITabItem;

interface ITabItemProps extends IBaseElementComponent, ITabItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<ITabItemProps, any> {
  protected baseClass = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      <Link label={this.props.label} />
      {this.props.dropdown || null}
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',

      // Disabled
      this.props.disabled ? 'uk-disabled' : '',
    ]);
  }
}
