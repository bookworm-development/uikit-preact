import { Component, h, observer } from '../../core';

@observer
export class Span extends Component<any, any> {
  public render() {
    return <div
      className={`uk-column-span`} >
      {this.props.children}
    </div>;
  }
}
