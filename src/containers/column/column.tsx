import { computed, ContainerComponent, h, IBaseContainerComponent, observer, toWidthClasses } from '../../core';

@observer
export class Column extends ContainerComponent<IBaseContainerComponent, any> {
  protected baseClass = '';

  @computed
  protected get content() {
    return <div
    className={toWidthClasses(this.props.width, 'uk-column-')} >
    {this.props.children}
  </div>;
  }
}
