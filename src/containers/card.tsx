import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';
import { Image } from '../images';

// TODO: Add support for horizontal aligment

export interface ICardProps extends IBaseContainerComponent {
  styleType?: 'default' | 'primary' | 'secondary';
  size?: 'small' | 'large';
  hoverable?: boolean;

  header?: JSX.Element | JSX.Element[] | string;
  footer?: JSX.Element | JSX.Element[] | string;

  media?: IMedia;

  badge?: JSX.Element | string;
}

interface IMedia { top?: string; left?: string; right?: string; bottom?: string; }
type MediaDirection = keyof IMedia;

@observer
export class Card extends ContainerComponent<ICardProps, any> {
  public static ClassTitle = 'uk-card-title';

  private static ClassPrefix = 'uk-card-';
  private static ClassMediaPrefix = 'uk-card-media-';
  private static ClassBody = 'uk-card-body';
  private static ClassHeader = 'uk-card-header';
  private static ClassFooter = 'uk-card-footer';
  private static ClassBadge = 'uk-card-badge';

  protected baseClass = 'uk-card';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style
      this.props.styleType ? `${Card.ClassPrefix}${this.props.styleType}` : '',

      // Size
      this.props.size ? `${Card.ClassPrefix}${this.props.size}` : '',

      // Hover effect
      this.props.hoverable ?  `${Card.ClassPrefix}hover` : '',
    ]);
  }

  @computed
  protected get content() {
    return <div
      className={this.className} {...this.attributes}>
      {this.mediaLeft}
      {this.header}
      {this.mediaTop}
      <div className={Card.ClassBody}>
        {this.badge}
        {this.props.children}
      </div>
      {this.mediaRight}
      {this.mediaBottom}
      {this.footer}
    </div>;
  }

  @computed
  protected get header(): JSX.Element | null {
    const header: Array<JSX.Element | null | string> = typeof this.props.header === 'string' ?
      [
        this.props.header,
      ] :
      this.props.header instanceof Array ? this.props.header :
      this.props.header ? [this.props.header] :
      [null];

    if (header[0] === null) {
      return null;
    }

    return <div className={Card.ClassHeader}>
      { ...header }
    </div>;
  }

  @computed
  protected get footer(): JSX.Element | null {
    const footer: Array<JSX.Element | null | string> = typeof this.props.footer === 'string' ?
      [
        this.props.footer,
      ] :
      this.props.footer instanceof Array ? this.props.footer :
      this.props.footer ? [this.props.footer] :
      [null];

    if (footer[0] === null) {
      return null;
    }

    return <div className={Card.ClassFooter}>
      { ...footer }
    </div>;
  }

  @computed
  protected get badge(): JSX.Element | null {
    return !this.props.badge ?
      null :
      <div className={`${Card.ClassBadge}${ typeof this.props.badge === 'string' ? ' uk-label' : '' }`}>
        {this.props.badge}
      </div>;
  }

  @computed
  protected get mediaLeft() {
    return this.media('left');
  }

  @computed
  protected get mediaTop() {
    return this.media('top');
  }

  @computed
  protected get mediaRight() {
    return this.media('right');
  }

  @computed
  protected get mediaBottom() {
    return this.media('bottom');
  }

  private media(direction: MediaDirection) {
    if (this.props.media && this.props.media[direction]) {
      const m = this.props.media[direction] as string;

      return <div className={`${Card.ClassMediaPrefix}${direction}`}>
        <Image src={m} alt='' />
      </div>;
    }

    return null;
  }
}
