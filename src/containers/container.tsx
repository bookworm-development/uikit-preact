import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

export interface IContainerProps extends IBaseContainerComponent {
  size?: 'xsmall' | 'small' | 'large' | 'expand';
}

@observer
export class Container extends ContainerComponent<IContainerProps, any> {
  protected baseClass: string = 'uk-container';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Container size
      this.props.size ? `uk-container-${this.props.size}` : '',
    ]);
  }

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
