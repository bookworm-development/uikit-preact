import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

export interface ITileProps extends IBaseContainerComponent {
  styleType?: 'default' | 'muted' | 'primary' | 'secondary';
}

@observer
export class Tile extends ContainerComponent<ITileProps, any> {
  protected baseClass: string = 'uk-tile';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Tile style
      this.props.styleType ? `uk-tile-${this.props.styleType}` : '',
    ]);
  }

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
