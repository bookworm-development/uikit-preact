import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

@observer
export class Div extends ContainerComponent<IBaseContainerComponent, any> {
  protected baseClass: string = '';

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
