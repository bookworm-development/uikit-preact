import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

export interface IGridProps extends IBaseContainerComponent {
  size?: 'small' | 'medium' | 'large' | 'collapse';
  divider?: boolean;
  heightMatchAll?: boolean;
  inDrop?: boolean;
  options?: IGridOptions;
}

interface IGridOptions {
  ['first-column']?: string;
  margin?: string;
  masonry?: boolean;
  parallax?: number;
}

@observer
export class Grid<P extends IGridProps, S> extends ContainerComponent<P, S> {
  public static ClassItemMatch = '';

  private static ClassPrefix = 'uk-grid-';
  private static ClassDivider = 'uk-grid-divider';
  private static ClassMatchAll = 'uk-grid-match';

  protected baseClass: string = '';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Grid size/style
      this.props.size ? `${Grid.ClassPrefix}${this.props.size}` : '',

      // Divider
      this.props.divider ? Grid.ClassDivider : '',

      // MatchAll
      this.props.heightMatchAll ? Grid.ClassMatchAll : '',

      // Stack grids in drops
      this.props.inDrop ? 'uk-drop-grid' : '',

      // Safety measure for defered JS loading
      'uk-grid',
    ]);
  }

  protected get attributes() {
    return {
      ...super.attributes,

      // Enable grid functionality
      ['uk-grid']: this.toAttributeValue(
        {
          ['first-column']: 'uk-first-column',
          margin: 'uk-grid-margin',
          masonry: false,
          parallax: 0,
        },
        (this.props.options || {}) as IGridOptions,
      ),
    };
  }

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
