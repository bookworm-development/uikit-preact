import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

export interface ISectionProps extends IBaseContainerComponent {
  styleType?: 'default' | 'muted' | 'primary' | 'secondary';
  preserveColor?: boolean;
  size?: 'xsmall' | 'small' | 'large' | 'xlarge';
}

@observer
export class Section extends ContainerComponent<ISectionProps, any> {
  protected baseClass: string = 'uk-section';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Section style
      this.props.styleType ? `uk-section-${this.props.styleType}` : '',

      // Section size
      this.props.size ? `uk-section-${this.props.size}` : '',

      // Preserve color
      this.props.preserveColor ? 'uk-preserve-color' : '',
    ]);
  }

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
