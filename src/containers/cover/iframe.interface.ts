export interface IIFrame {
  src: string;
  width?: number;
  height?: number;
  frameborder?: number;
  allowfullscreen?: boolean;
  name?: string;
  type: 'IFrame';
}
