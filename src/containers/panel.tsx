import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';

export interface IPanelProps extends IBaseContainerComponent {
  scrollable?: boolean;
}

@observer
export class Panel extends ContainerComponent<IPanelProps, any> {
  private static ClassScrollable = 'uk-panel-scrollable';

  protected baseClass: string = 'uk-panel';

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Scrollable
      this.props.scrollable ? Panel.ClassScrollable : '',
    ]);
  }

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
