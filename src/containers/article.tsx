import {
  computed, ContainerComponent, h, IBaseContainerComponent, observer,
} from '../core';
import { Link } from '../link';
import { Grid } from './grid';

export interface IArticleProps extends IBaseContainerComponent {
  title?: JSX.Element | JSX.Element[] | string;
  meta?: JSX.Element | JSX.Element[] | string;
  lead?: JSX.Element | JSX.Element[] | string;

  links?: IArticleLink[];
}

@observer
export class Article extends ContainerComponent<IArticleProps, any> {
  protected baseClass: string = 'uk-article';

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.title}
      {this.meta}
      {this.lead}
      {this.props.children}
      {this.links}
    </div>;
  }

  @computed
  protected get title(): JSX.Element | null {
    const title: Array<JSX.Element | null> = typeof this.props.title === 'string' ?
      [
        <a className='uk-link-reset' href=''>{this.props.title}</a>,
      ] :
      this.props.title instanceof Array ? this.props.title :
      this.props.title ? [this.props.title] :
      [null];

    if (title[0] === null) {
      return null;
    }

    return <h1 className='uk-article-title'>
      { ...title }
    </h1>;
  }

  @computed
  protected get meta(): JSX.Element | null {
    const meta: Array<JSX.Element | null | string> = typeof this.props.meta === 'string' ?
      [
        this.props.meta,
      ] :
      this.props.meta instanceof Array ? this.props.meta :
      this.props.meta ? [this.props.meta] :
      [null];

    if (meta[0] === null) {
      return null;
    }

    return <p className='uk-article-meta'>
      { ...meta }
    </p>;
  }

  @computed
  protected get lead(): JSX.Element | null {
    const lead: Array<JSX.Element | null | string> = typeof this.props.lead === 'string' ?
      [
        this.props.lead,
      ] :
      this.props.lead instanceof Array ? this.props.lead :
      this.props.lead ? [this.props.lead] :
      [null];

    if (lead[0] === null) {
      return null;
    }

    // TODO: Replace lead with a Text.Lead component!
    return <p className='uk-text-lead'>
      { ...lead }
    </p>;
  }

  @computed
  protected get links(): JSX.Element | null {
    if (!this.props.links) {
      return null;
    }

    return <Grid size={'small'} width={'child-auto'}>
      {
        this.props.links.map((l) => {
          return <div>
            <Link {...l} class={'uk-button uk-button-text'} />
          </div>;
        })
      }
    </Grid>;
  }
}

interface IArticleLink {
  location: string;
  label: string;
}
