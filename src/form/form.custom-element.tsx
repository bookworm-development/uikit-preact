import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ICustomFieldProps extends IBaseElementComponent {
  generic?: boolean;
  options?: ICustomFieldOptions;
}

interface ICustomFieldOptions { target?: string | boolean; }

@observer
export class CustomField extends ElementComponent<ICustomFieldProps, any> {
  protected baseClass: string = '';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Custom field
      ...this.props.generic ? {} : {
        ['uk-form-custom']: this.toAttributeValue(
            {
              target: false,
            },
            this.props.options as ICustomFieldOptions,
          ),
        },
    };
  }
}
