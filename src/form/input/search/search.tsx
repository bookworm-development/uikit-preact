import {
  computed, FormComponent, h, IBaseFormComponent, observer,
} from '../../../core';
import { Form } from '../../form';
import { Input } from '../input';

export interface ISearchProps extends IBaseFormComponent {
  searchIcon?: boolean;
  searchIconFlip?: boolean;
  searchIconToggle?: boolean;
  styleType?: SearchStyleType;
}

type SearchStyleType = 'default' | 'large' | 'navbar';

@observer
export class Search extends FormComponent<ISearchProps, any> {
  protected baseClass: string = 'uk-search';

  public render() {
    return <Form forceAttributes={{
        class: `uk-search uk-width-1-1${
          this.props.styleType ?
            ` uk-search-${this.props.styleType}` :
            ''
          }`,
      }}>
      {
        this.props.searchIcon ?
          <a uk-search-icon className={this.iconClassName} /> : null
      }
      <Input
        {...this.attributes}
        {...this.props}
        type={'search'}
        forceAttributes={{ className: 'uk-search-input' }} />
    </Form>;
  }

  @computed
  private get iconClassName() {
    return this.toClassName([
      this.props.searchIconFlip ? 'uk-search-icon-flip' : '',
      this.props.searchIconToggle ?
        this.props.styleType === 'navbar' ?
          'uk-navbar-toggle uk-toggle' :
          'uk-search-toggle' :
        '',
    ]);
  }
}
