import { FormComponent, h, IBaseFormComponent, observer } from '../../../core';

export interface IRadioProps extends IBaseFormComponent {
  content?: string;
}

@observer
export class Radio extends FormComponent<IRadioProps, any> {
  protected baseClass: string = 'uk-radio';

  public render() {
    return <input className={this.className} {...this.attributes} type='radio'
      name={this.props.name} value={this.props.content}
      checked={!!this.props.value} />;
  }
}
