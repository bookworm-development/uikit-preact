import { computed, ElementChild, FormComponent, h, IBaseFormComponent, observer } from '../../../core';
import { Label } from '../../form.label';

export interface ICheckboxProps extends IBaseFormComponent {
  content?: string;
  indeterminate?: boolean;
  label?: ElementChild;
}

@observer
export class Checkbox extends FormComponent<ICheckboxProps, any> {
  protected baseClass: string = 'uk-checkbox';

  public render() {
    const checkbox = <input className={this.className} {...this.attributes} type='checkbox'
      name={this.props.name} value={this.props.content}
      checked={this.props.value.get()} />;

    if (this.props.label) {
      return <Label label={this.props.label} childrenAlign='left' margin='small-right'>{checkbox}</Label>;
    }

    return checkbox;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Indeterminate
      indeterminate: !!this.props.indeterminate,
    };
  }
}
