import { Icons } from '../..';
import { computed, ElementChild, FormComponent, h, IBaseFormComponent, IconType, observer } from '../../core';
import { CustomField } from '../form.custom-element';

export interface IInputProps extends IBaseFormComponent {
  placeholder?: string;

  icon?: IconType;
  iconAlign?: 'left' | 'right';
  iconElement?: ElementChild;

  type?: 'text' | 'color' | 'email' | 'file' | 'image' | 'number' | 'password' | 'url' | 'search';
}

@observer
export class Input extends FormComponent<IInputProps, any> {
  protected baseClass: string = 'uk-input';

  public render() {
    if (this.props.icon) {
      return <CustomField generic inline width='1-1'>
        {
          this.props.iconElement ?
            this.props.iconElement :
            <Icons.Icon icon={this.props.icon} tabindex={-1}
              class={`uk-form-icon${this.props.iconAlign === 'right' ? ' uk-form-icon-flip' : ''}`} />
        }
        { this.input }
      </CustomField>;
    }

    return this.input;
  }

  @computed
  protected get input() {
    return <input className={this.className} {...this.attributes} type={this.props.type || 'text'}
      name={this.props.name} placeholder={this.props.placeholder || ''}
      value={this.props.value as unknown as string} />;
  }
}
