import { ButtonStyle } from '../button/button';
import {
  computed, ElementChild, ElementComponent, h, IBaseElementComponent, MouseActionEvent, observer,
} from '../core';
import { Toggle, toToggleAttribute } from '../core/toggle';

export interface ILinkProps extends IBaseElementComponent {
  label?: ElementChild;
  location?: string;

  handleOn?: MouseActionEvent;
  handler?: (ev: MouseEvent) => void;

  styleType?: LinkStyle | LinkStyle[];
  button?: boolean | ButtonStyle;
  toggle?: Toggle;
  toTop?: boolean;
  scroll?: boolean;
}

type LinkStyle = 'muted' | 'text' | 'heading' | 'reset';

@observer
export class Link extends ElementComponent<ILinkProps, any> {
  private static ClassPrefix = 'uk-link-';

  protected baseClass = '';

  public render() {
    return <a class={this.className} {...this.attributes} href={this.props.location}>{this.props.label}</a>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style
      ...this.linkStyle,

      // Button styled
      ...this.props.button ? ['uk-button', `uk-button-${this.props.styleType || 'default'}`] : [],
    ]);
  }

  @computed get attributes() {
    const toggleAttrs = toToggleAttribute(this.props.toggle);

    return {
      ...super.attributes,

      // Toggle
      ...this.props.toggle ? { ['uk-toggle']: this.toAttributeValue(toggleAttrs, toggleAttrs, true) } : {},

      // ToTop
      ...this.props.toTop ? { ['uk-totop']: true } : {},

      // Scroll (allows smooth scrolling to the specified #location in page)
      ...this.props.scroll ? { ['uk-scroll']: true } : {},

      // Handle events with outside handler
      ...this.props.handler ?
        {
          [this.props.handleOn || 'onClick']: this.props.handler,
        } : {},
    };
  }

  @computed
  private get linkStyle() {
    if (this.props.styleType) {
      return (this.props.styleType instanceof Array ? this.props.styleType : [this.props.styleType]).map((s) => {
        return `${Link.ClassPrefix}${s}`;
      });
    }

    return [];
  }
}
