import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IOverlayProps extends IBaseElementComponent {
  styleType?: 'default' | 'primary';
}

@observer
export class Overlay extends ElementComponent<IOverlayProps, any> {
  protected baseClass = 'uk-overlay';

  public render() {
    return <div className={this.className} {...this.attributes}>{this.props.children}</div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Style type
      this.props.styleType ? `uk-overlay-${this.props.styleType}` : '',
    ]);
  }
}
