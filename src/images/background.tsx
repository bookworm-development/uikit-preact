import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

interface IImageProps extends IBaseElementComponent {
  src: string;

  alt?: string;
  srcset?: string;
  size?: { height?: number | string; width?: number | string; };
  sizes?: string;
  target?: string;
}

@observer
export class Background extends ElementComponent<IImageProps, any> {
  protected baseClass = '';

  public render() {
    return <img class={this.className} {...this.attributes}/>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Source
      ['data-src']: this.props.src,

      // Sources set
      ...this.props.srcset ? { ['data-srcset']: this.props.srcset } : {},

      // Size
      ...this.props.size ? this.props.size : {},

      // Sizes (source set sizes)
      ...this.props.sizes ? { sizes: this.props.sizes } : {},

      // Alt
      ...this.props.alt ? { alt: this.props.alt } : {},

      // Target
      ['uk-img']: this.toAttributeValue({ target: '' }, this.props),
    };
  }
}
