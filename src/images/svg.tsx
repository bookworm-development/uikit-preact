import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ISVGProps extends IBaseElementComponent {
  src: string;
  symbolId?: string;
  preserve?: boolean;
}

@observer
export class SVG extends ElementComponent<ISVGProps, any> {
  protected baseClass = '';

  public render() {
    return <img class={this.className} {...this.attributes}/>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Preserve
      ...this.props.preserve ? {['uk-preserve']: true} : {},

      // Src and symbol id
      src: `${this.props.src}${this.props.symbolId ? '#' + this.props.symbolId : ''}`,

      // Make it an inline svg
      ['uk-svg']: true,
    };
  }
}
