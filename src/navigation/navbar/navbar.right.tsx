import { ElementComponent, h, IBaseElementComponent, observer } from '../../core';

@observer
export class NavBarRight extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass: string = 'uk-navbar-right';

  public render() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }
}
