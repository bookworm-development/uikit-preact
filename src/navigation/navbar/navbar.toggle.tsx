import { action, computed, ElementComponent, h, IBaseElementComponent, observable, observer } from '../../core';
import { Link } from '../../link';

export interface INavToggle extends IBaseElementComponent {
  label?: string;
  handler: (openStatus: boolean) => void;
}

@observer
export class NavBarToggle extends ElementComponent<INavToggle, any> {
  protected baseClass: string = 'uk-navbar-toggle';

  @observable
  private open: boolean = false;

  public render() {
    return <Link
      class={this.className} forceAttributes={this.attributes} label={this.props.label || ''} location={'#'} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      onClick: this.handle,

      ['uk-navbar-toggle-icon']: true,
    };
  }

  @action.bound
  private handle() {
    this.open = !this.open;
    this.props.handler(this.open);
  }
}
