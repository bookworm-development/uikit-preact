import { computed, ElementComponent, h, IBaseElementComponent, Icon, IconType, observer } from '../../core';
import { Link } from '../../link';

interface IDotNavItem extends IBaseElementComponent {
  location?: string;
}

export type DotNavItem = IDotNavItem;

export interface IDotNavItemProps extends IDotNavItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<IDotNavItemProps, any> {
  protected baseClass: string = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      <Link location={this.props.location || '#'} />
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',
    ]);
  }
}
