import { INavHeader } from './header';
import { INavItem } from './item';
import { INavSeparator } from './separator';

export type NavItems = INavItem | INavHeader | INavSeparator;

export { Header } from './header';
export { Item } from './item';
export { Separator } from './separator';
