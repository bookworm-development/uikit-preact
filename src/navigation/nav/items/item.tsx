import { NavItems } from '.';
import { computed, ElementComponent, h, IBaseElementComponent, observer, Omit } from '../../../core';
import { Icon, IconType } from '../../../core/icons';
import { Link } from '../../../link';
import { Nav } from '../nav';

export interface INavItem extends IBaseElementComponent {
  items?: NavItems[];
  icon?: IconType;
  label: string;
  location?: string;
  type: 'item';
}

export interface INavItemProps extends INavItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<Omit<INavItemProps, 'type'>, any> {
  protected baseClass: string = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      {
        !this.props.items ?
          [
            <Link location={this.props.location || '#'} label={[this.icon, this.props.label]} />,
          ] :
          [
            <Link location={this.props.location || '#'} label={[this.icon, this.props.label]} />,
            <Nav nested items={this.props.items} />,
          ]
      }
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',

      // Parent
      this.props.items ? 'uk-parent' : '',
    ]);
  }

  @computed
  protected get icon() {
    return this.props.icon ?
      <Icon icon={this.props.icon} class={'uk-margin-small-right'} /> : null;
  }
}
