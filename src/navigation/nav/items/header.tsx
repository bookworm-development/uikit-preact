import { ElementComponent, h, IBaseElementComponent, observer, Omit } from '../../../core';

export interface INavHeader extends IBaseElementComponent {
  label: string | JSX.Element | JSX.Element[];
  type: 'header';
}

@observer
export class Header extends ElementComponent<Omit<INavHeader, 'type'>, any> {
  protected baseClass = 'uk-nav-header';

  public render() {
    return <li className={this.className} {...this.attributes}>
      {this.props.label}
      </li>;
  }
}
