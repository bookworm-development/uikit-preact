import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { Link } from '../../link';

export interface ISlideNavPreviousProps extends IBaseElementComponent {
  large?: boolean;
  slider?: 'slider' | 'slideshow';
}

@observer
export class Previous extends ElementComponent<ISlideNavPreviousProps, any> {
  protected baseClass: string = '';

  public render() {
    return <Link location={'#'} class={this.className} {...this.attributes} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Previous slide navigation functionality
      ['ul-slidenav-previous']: true,

      // Make the slidenav available for sliders also
      ...this.props.slider ?
        this.props.slider === 'slider' ?
          { ['uk-slider-item']: 'previous' } :
          { ['uk-slideshow-item']: 'previous' } :
          {},
    };
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Large
      this.props.large ? 'uk-slidenav-large' : '',
    ]);
  }
}
