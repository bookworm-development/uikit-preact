import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { IconNavItem, Item } from './iconnav.item';

export interface IIconNavProps extends IBaseElementComponent {
  active?: number;
  items: IconNavItem[];
  vertical?: boolean;
}

@observer
export class IconNav extends ElementComponent<IIconNavProps, any> {
  protected baseClass: string = 'uk-iconnav';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      { this.props.items.map((i, ii) => <Item {...i} active={ii === this.props.active} />) }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Vertical orientation
      this.props.vertical ? 'uk-iconnav-vertical' : '',
    ]);
  }
}
