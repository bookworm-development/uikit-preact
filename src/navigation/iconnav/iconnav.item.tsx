import { computed, ElementComponent, h, IBaseElementComponent, Icon, IconType, observer } from '../../core';
import { Link } from '../../link';

interface IIconNavItem extends IBaseElementComponent {
  location?: string;
  icon: IconType;
}

export type IconNavItem = IIconNavItem;

export interface IIconNavItemProps extends IIconNavItem {
  active?: boolean;
}

@observer
export class Item extends ElementComponent<IIconNavItemProps, any> {
  protected baseClass: string = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      <Link location={this.props.location || '#'} label={<Icon icon={this.props.icon} />}/>
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Active
      this.props.active ? 'uk-active' : '',
    ]);
  }
}
