import {  ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { SubNav } from '../navigation';
import { SubNavItem } from '../navigation/subnav/subnav.item';

interface ICommentMetaProps extends IBaseElementComponent {
  meta: SubNavItem[];
}

@observer
export class Meta extends ElementComponent<ICommentMetaProps, any> {
  protected baseClass = 'uk-comment-meta';

  public render() {
    return <SubNav
      divider margin='remove-top'
      items={this.props.meta}
      class={this.className}
      forceAttributes={this.attributes} />;
  }
}
