import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Comment } from './comment';

interface ICommentsListProps extends IBaseElementComponent {
  items: IItem[];
  secondary?: boolean;
}

interface IItem {
  comment: ICommentsListProps;
  items?: IItem[];
}

@observer
export class List extends ElementComponent<ICommentsListProps, any> {
  protected baseClass = '';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      {
        this.props.items.map((i) => <li>
          <Comment {...i.comment} />
          { i.items ? <List items={i.items} secondary /> : null }
        </li>)
      }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Secondary lists should have no class
      this.props.secondary ? '' : 'uk-comment-list',
    ]);
  }
}
