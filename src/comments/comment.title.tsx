import { Headings } from '..';
import {  ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Link } from '../link';
import { ILinkProps } from '../link/link';

interface ICommentHeaderProps extends IBaseElementComponent {
  title: ILinkProps;
}

@observer
export class Title extends ElementComponent<ICommentHeaderProps, any> {
  protected baseClass = 'uk-comment-title';

  public render() {
    return <Headings.H4 class={this.className} {...this.attributes}>
      <Link  styleType='reset' {...this.props.title} />
    </Headings.H4>;
  }
}
