import { Grid } from '../containers';
import { IGridProps } from '../containers/grid';
import { h, observer } from '../core';

@observer
export class Header extends Grid<IGridProps, any> {
  protected baseClass = 'uk-comment-header';

  public render() {
    return <header className={this.className || ''} {...this.attributes}>
      {this.props.children}
    </header>;
  }
}
