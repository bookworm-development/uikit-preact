import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Link } from '../link';

export interface IPaginationProps extends IBaseElementComponent {
  active?: number;
  pages: IItem[];
  next?: boolean | IItem;
  previous?: boolean | IItem;
}

interface IItem {
  label: number | string;
  disabled?: boolean;
}

@observer
export class Pagination extends ElementComponent<IPaginationProps, any> {
  public static generatePages(max: number, min: number = 1) {
    return new Array(max).fill(0).map((_, i) => ({ label: i + min }));
  }

  protected baseClass = 'uk-pagination';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      { this.previous }
      {
        this.props.pages.map((p, i) => {
          return <li
            className={`${i === this.props.active ? 'uk-active' : ''}${p.disabled ? 'uk-disabled' : ''}`}>
            <Link label={<span>{p.label}</span>} />
          </li>;
        })
      }
      { this.next }
    </ul>;
  }

  @computed
  protected get previous() {
    if (this.props.previous) {
      const prev = typeof this.props.previous === 'boolean' ? { label: '', disabled: false } : this.props.previous;

      return <li>
        <Link location='#' label={
          [
            prev.label as string || null,
            <span className={`${prev.label ? 'uk-margin-small-left' : ''}`} uk-pagination-previous></span>,
          ]
        } />
      </li>;
    }

    return null;
  }

  @computed
  protected get next() {
    if (this.props.next) {
      const prev = typeof this.props.next === 'boolean' ? { label: '', disabled: false } : this.props.next;

      return <li>
        <Link location='#' label={
          [
            <span className={`${prev.label ? 'uk-margin-small-right' : ''}`} uk-pagination-next></span>,
            prev.label as string || null,
          ]
        } />
      </li>;
    }

    return null;
  }
}
