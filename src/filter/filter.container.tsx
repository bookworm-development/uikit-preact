import { computed, ContainerComponent, h, IBaseContainerComponent, observer } from '../core';

interface IFilterContainerProps extends IBaseContainerComponent {
  target?: string;
  selActive?: boolean | string;
}

@observer
export class Container extends ContainerComponent<IFilterContainerProps, any> {
  protected baseClass = '';

  @computed
  protected get content() {
    return <div className={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Filter
      ['uk-filter']: this.toAttributeValue(
        {
          selActive: false,
          target: '.js-filter',
        },
        this.props,
        true,
      ),
    };
  }
}
