import { ElementComponent, h, observer, Omit } from '../core';
import { ISubNavProps, SubNav } from '../navigation/subnav/subnav';
import { Control } from './filter.control';

interface IFilterControlsProps extends Omit<ISubNavProps, 'items'> {
  categories: ICategory[];
}

interface ICategory {
  label: string;
  filter?: IFilter;
  sort?: ISort;
  group?: string;
}

export interface IFilter { attribute: string; value: string; matchContaining?: boolean;  }
export interface ISort { attribute: string; order?: 'asc' | 'desc'; }

@observer
export class Controls extends ElementComponent<IFilterControlsProps, any> {
  protected baseClass = '';

  public render() {
    return <SubNav class={this.className} {...this.attributes} items={[]}>
      {
        this.props.categories.map((c) => <Control label={c.label} filter={c.filter} group={c.group} sort={c.sort} />)
      }
    </SubNav>;
  }
}
