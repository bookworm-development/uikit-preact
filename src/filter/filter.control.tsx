import { computed, ElementComponent, h, observer } from '../core';
import { ISubNavItemProps, Item } from '../navigation/subnav/subnav.item';
import { IFilter, ISort } from './filter.controls';

interface IControlProps extends ISubNavItemProps {
  label: string;
  filter?: IFilter;
  sort?: ISort;
  group?: string;
}

@observer
export class Control extends ElementComponent<IControlProps, any> {
  protected baseClass = '';

  public render() {
    return <Item label={this.props.label} forceAttributes={this.attributes} class={this.className} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Filter
      ['uk-filter-control']: this.filter,
    };
  }

  @computed
  protected get filter() {
    let filter: string = '';
    let sort: string = '';

    if (this.props.filter) {
      const f = this.props.filter;
      filter = `[data-${f.attribute}${f.matchContaining ? '*' : ''}='${f.value}']`;
    }

    if (this.props.sort) {
      const s = this.props.sort;
      sort = `sort: ${s.attribute}${s.order && s.order === 'desc' ? '; order: desc' : ''}`;
    }

    return [
      `filter: ${filter}`,
      this.props.group ? `group: ${this.props.group}` : '',
      sort,
    ].filter((v) => v.length).join('; ');
  }
}
