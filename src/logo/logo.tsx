import { ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Link } from '../link';

interface ILogoProps extends IBaseElementComponent {
  location?: string;
}

@observer
export class Logo extends ElementComponent<ILogoProps, any> {
  protected baseClass = 'uk-logo';

  public render() {
    const label: JSX.Element = this.props.children ?
      typeof this.props.children === 'string' ?
        <span>{this.props.children}</span> :
          this.props.children as JSX.Element :
        <span></span>;

    return <Link location={this.props.location || '#'} label={label} class={this.className} {...this.attributes} />;
  }
}
