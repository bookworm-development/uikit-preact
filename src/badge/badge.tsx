import { ElementComponent, h, IBaseElementComponent, observer } from '../core';

@observer
export class Badge extends ElementComponent<IBaseElementComponent, any> {
  protected baseClass = 'uk-badge';

  public render() {
    return <span class={this.className} {...this.attributes}>
      {this.props.children}
    </span>;
  }
}
