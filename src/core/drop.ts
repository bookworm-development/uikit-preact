export type DropPosition =
  'top-left' | 'top-center' | 'top-right' | 'top-justify' |
  'left-top' | 'left-center' | 'left-bottom' |
  'right-top' | 'right-center' | 'right-bottom' |
  'bottom-left' | 'bottom-center' | 'bottom-right' | 'bottom-justify';

export type DropMode = 'click' | 'hover' | 'click, hover';

export interface IDrop {
  pos?: DropPosition;
  mode?: DropMode;
  delayShow?: number;
  delayHide?: number;
  boundary?: string;
  boundaryAlign?: boolean;
  flip?: boolean | 'x' | 'y';
  offset?: number;
}

export type Drop = IDrop;
