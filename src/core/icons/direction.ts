export enum IconsDirection {
  reply = 'reply',
  forward = 'forward',
  expand = 'expand',
  shrink = 'shrink',
  'arrow-up' = 'arrow-up',
  'arrow-down' = 'arrow-down',
  'arrow-left' = 'arrow-left',
  'arrow-right' = 'arrow-right',
  'chevron-up' = 'chevron-up',
  'chevron-down' = 'chevron-down',
  'chevron-left' = 'chevron-left',
  'chevron-right' = 'chevron-right',
  'chevron-double-left' = 'chevron-double-left',
  'chevron-double-right' = 'chevron-double-right',
  'triangle-up' = 'triangle-up',
  'triangle-down' = 'triangle-down',
  'triangle-left' = 'triangle-left',
  'triangle-right' = 'triangle-right',
}
