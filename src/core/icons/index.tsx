import { IconsApp } from './app';
import { IconsBrands } from './brands';
import { IconsDevices } from './devices';
import { IconsDirection } from './direction';
import { IconsEditor } from './editor';
import { IconsStorage } from './storage';

export const Icons = {
  app: IconsApp,
  brands: IconsBrands,
  devices: IconsDevices,
  direction: IconsDirection,
  editor: IconsEditor,
  storage: IconsStorage,
};

export type IconType = IconsApp | IconsBrands | IconsDevices | IconsDirection | IconsEditor | IconsStorage;

export { Icon } from './icon';
