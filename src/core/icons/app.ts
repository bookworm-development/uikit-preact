export enum IconsApp {
  home = 'home',
  'sign-in' = 'sign-in',
  'sign-out' = 'sign-out',
  user = 'user',
  users = 'users',
  lock = 'lock',
  unlock = 'unlock',
  settings = 'settings',
  cog = 'cog',
  nut = 'nut',
  comment = 'comment',
  commenting = 'commenting',
  comments = 'comments',
  hashtag = 'hashtag',
  tag = 'tag',
  cart = 'cart',
  'credit-card' = 'credit-card',
  mail = 'mail',
  receiver = 'receiver',
  print = 'print',
  search = 'search',
  location = 'location',
  bookmark = 'bookmark',
  code = 'code',
  'paint-bucket' = 'paint-bucket',
  camera = 'camera',
  'video-camera' = 'video-camera',
  bell = 'bell',
  microphone = 'microphone',
  bolt = 'bolt',
  star = 'star',
  heart = 'heart',
  happy = 'happy',
  lifesaver = 'lifesaver',
  rss = 'rss',
  social = 'social',
  'git-branch' = 'git-branch',
  'git-fork' = 'git-fork',
  world = 'world',
  calendar = 'calendar',
  clock = 'clock',
  history = 'history',
  future = 'future',
  pencil = 'pencil',
  trash = 'trash',
  move = 'move',
  link = 'link',
  question = 'question',
  info = 'info',
  warning = 'warning',
  image = 'image',
  thumbnails = 'thumbnails',
  table = 'table',
  list = 'list',
  menu = 'menu',
  grid = 'grid',
  more = 'more',
  'more-vertical' = 'more-vertical',
  plus = 'plus',
  'plus-circle' = 'plus-circle',
  minus = 'minus',
  'minus-circle' = 'minus-circle',
  close = 'close',
  check = 'check',
  ban = 'ban',
  refresh = 'refresh',
  play = 'play',
  'play-circle' = 'play-circle',
  spinner = 'spinner',
  marker = 'marker',
}
