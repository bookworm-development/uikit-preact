export enum IconsEditor {
  bold = 'bold',
  italic = 'italic',
  strikethrough = 'strikethrough',
  'quote-right' = 'quote-right',
}
