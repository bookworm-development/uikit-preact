export enum IconsDevices {
  tv = 'tv',
  desktop = 'desktop',
  laptop = 'laptop',
  tablet = 'tablet',
  phone = 'phone',
  'tablet-landscape' = 'tablet-landscape',
  'phone-landscape' = 'phone-landscape',
}
