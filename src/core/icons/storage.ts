export enum IconsStorage {
  file = 'file',
  'file-text' = 'file-text',
  'file-pdf' = 'file-pdf',
  copy = 'copy',
  'file-edit' = 'file-edit',
  folder = 'folder',
  album = 'album',
  push = 'push',
  pull = 'pull',
  server = 'server',
  database = 'database',
  'cloud-upload' = 'cloud-upload',
  'cloud-download' = 'cloud-download',
  download = 'download',
  upload = 'upload',
}
