export type ValueUpdateEvent =
  'onChange' | 'onChangeCapture' | 'onInput' | 'onKeyDown' | 'onKeyDownCapture' | 'onKeyPress' |
  'onKeyPressCapture' | 'okKeyUp' | 'onKeyUpCapture';

export type MouseActionEvent = 'onMouseDown' | 'onMouseUp' | 'onClick';
