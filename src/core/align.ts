export type AlignElement = 'left' | 'right' | 'center';

export type AlignElementResponsive =
  'left@s' | 'left@m' | 'left@l' | 'left@xl' |
  'right@s' | 'right@m' | 'right@l' | 'right@xl';

export type Alignment =
  AlignElement | AlignElementResponsive;

// Generates a class name based on provided width(s)
export function toAlignClasses(
  alignement: Alignment | Alignment[] | undefined,
  prefix: string = '') {
  const a: Alignment[] = alignement ?
    typeof alignement === 'string' ? [alignement] : alignement : [];

  return a.
    filter((aa) => !!aa).
    map((aa) => `${ prefix ? prefix : 'uk-align-' }${aa}`).join(' ');
}
