export type BackgroundColor = 'default' | 'muted' | 'primary' | 'secondary';

export type BackgroundModifiers = 'cover' | 'contain';

export type BackgroundPosition =
  'top-left' | 'top-center' | 'top-right' |
  'center-left' | 'center-center' | 'center-right' |
  'bottom-left' | 'bottom-center' | 'bottom-right';

export type BackgroundRepeat = 'norepeat';

export type BackgroundAttachment = 'fixed';

export type BackgroundResponsive =
  'image@s' | 'image@m' | 'image@l' | 'image@xl';

export type BackgroundBlend =
  'blend-multiply' | 'blend-screen' | 'blend-overlay' | 'blend-darken' | 'blend-lighten' |
  'blend-color-dodge' | 'blend-color-burn' | 'blend-hard-light' | 'blend-soft-light' |
  'blend-difference' | 'blend-exclusion' | 'blend-hue' | 'blend-saturation' |
  'blend-color' | 'blend-luminosity';

export type Background =
  BackgroundColor | BackgroundModifiers | BackgroundPosition |
  BackgroundRepeat | BackgroundAttachment | BackgroundResponsive |
  BackgroundBlend;

// Generates a class name based on provided background(s)
export function toBackgroundClasses(background: Background | Background[] | undefined) {
  const b: Background[] = background ?
    typeof background === 'string' ? [background] : background : [];

  return b.
    filter((bb) => bb.length).
    map((bb) => `uk-background-${bb}`).
    join(' ');
}

export function toBackgroundAttributes(backgroundImage: string | undefined) {
  if (backgroundImage) {
    return {
      ['background-image']: `url(${backgroundImage})`,
    };
  }

  return {};
}
