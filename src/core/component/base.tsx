import { Component } from 'preact';

export interface IBaseComponent {
  class?: string | string[];
  cursor?: CursorType;
  display?: 'block' | 'inline' | 'inline-block';
  forceAttributes?: IAttributeDefaults;
  style?: { [prop: string]: any; };
  tabindex?: number;
  userSelect?: UserSelectType;
}

type CursorType =
  'auto' | 'default' | 'none' | // General
  'context-menu' | 'help' | 'pointer' | 'progress' | 'wait' | // Link/status
  'cell' |  'crosshair' | 'text' | 'vertical-text' | // Selection
  'alias' | 'copy' | 'move' | 'no-drop' | 'not-allowed' | 'grab' | 'grabbing' | // Drag-drop
  // Resize/scrolling
  'all-scroll' | 'col-resize' | 'row-resize' | 'n-resize' | 'e-resize' | 's-resize' | 'w-resize' |
  'ne-resize' | 'nw-resize' | 'se-resize' | 'sw-resize'  | 'ew-resize' | 'ns-resize'  |
  'nesw-resize' | 'nwse-resize'  |
  // Zooming
  'zoom-in' | 'zoom-out';

type UserSelectType = 'none' | 'auto' | 'text' | 'all';

export abstract class BaseComponent<P extends IBaseComponent, S> extends Component<P, S> {
  public static asAttributeValue(defaults: IAttributeDefaults, options: IAttributeDefaults, enforceDefaults: boolean) {
    const attrs: IAttributeDefaults = {};

    Object.keys(defaults).forEach((a) => {
      attrs[a] = typeof options[a] !== 'undefined' ? options[a] : defaults[a];
    });

    let attrNames = Object.keys(attrs);

    if (!enforceDefaults) {
      attrNames = attrNames.filter((a) => attrs[a] !== defaults[a]);
    }

    return attrNames.map((a) => (`${a}: ${attrs[a as keyof IAttributeDefaults] as any}`)).join('; ');
  }

  // Base component class
  protected abstract baseClass: string = '';

  /**
   * Generates the list of class names that can be attached to a base component
   */
  protected get className() {
    const classes: string[] =
      (
        this.props.class ?
          (this.props.class as string[]).map ? this.props.class : [this.props.class] :
          []
      ) as string[];

    return this.toClassName([
      this.baseClass,

      // Extra classes added
      ...classes,

      // Display
      this.props.display ? `uk-display-${this.props.display}` : '',
    ]);
  }

  /**
   * Generates the attributes map associated with a base component
   */
  protected get attributes(): IAttributeDefaults {
    return {
      // Make sure to add the propagated attributes (for component compositions)
      ...typeof this.props.forceAttributes !== 'undefined' ? this.props.forceAttributes as IAttributeDefaults : {},

      // Style
      style: {
        ...this.props.style || {},

        // Cursor
        ...this.props.cursor ? { cursor: this.props.cursor } : {},

        // User-select
        ...this.props.userSelect ? { ['user-select']: this.props.userSelect } : {},
      },

      // TabIndex
      ...this.props.tabindex !== undefined ? { tabindex: this.props.tabindex } : {},
    };
  }

  /**
   * Given an array of classes filters out empty classes and generates the className string
   */
  protected toClassName(classes: string[]) {
    return classes.
      filter((c) => !!c.length).
      join(' ');
  }

  protected toAttributeValue(
    defaults: IAttributeDefaults, options: IAttributeDefaults, enforceDefaults: boolean = false) {
    return BaseComponent.asAttributeValue(defaults, options, enforceDefaults);
  }
}

interface IAttributeDefaults {
  [option: string]: any;
}
