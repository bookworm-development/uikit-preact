import { h } from '..';
import { Background, toBackgroundAttributes, toBackgroundClasses } from '../background';
import { Drop } from '../drop';
import { Toggle, toToggleAttribute } from '../toggle';
import { ElementComponent, IBaseElementComponent } from './element';

export interface IBaseContainerComponent extends IBaseElementComponent {
  loading?: boolean;
  spinner?: ISpinner;

  background?: Background | Background[];
  backgroundImage?: string;

  toggle?: Toggle;

  drop?: Drop;
  dropDown?: Drop;
}

export interface ISpinner {
  ratio?: number;
}

export abstract class ContainerComponent<P extends IBaseContainerComponent, S> extends ElementComponent<P, S> {
  public render() {
    return this.props.loading ?
      <div {...this.attributes} className={this.className}>
        <span
          {
            ...{
              ['uk-spinner']:
                `ratio: ${this.props.spinner && this.props.spinner.ratio || 1}`,
            }
          }>
        </span>
      </div> :
      this.content;
  }

  /**
   * Generates the list of class names that can be attached to a container component
   */
  protected get className() {
    return this.toClassName([
      super.className,

      ...this.props.loading ? ['uk-flex', 'uk-flex-middle', 'uk-flex-center'] : [],

      // Background
      toBackgroundClasses(this.props.background),
    ]);
  }

  /**
   * Generates the attributes map associated with an element component
   */
  protected get attributes() {
    const toggleAttrs = toToggleAttribute(this.props.toggle);

    return {
      ...super.attributes,

      style: {
        ...(super.attributes as any).style || {},
        ...this.props.style || {},

        // Background image
        ...toBackgroundAttributes(this.props.backgroundImage),
      },

      // Toggle
      ...this.props.toggle ? { ['uk-toggle']: this.toAttributeValue(toggleAttrs, toggleAttrs, true) } : {},

      // Drop and dropdown
      ...this.props.dropDown || this.props.drop ? {
        [this.props.dropDown ? 'uk-dropdown' : 'uk-drop']: this.toAttributeValue({
          boundary: 'window',
          boundaryAlign: false,
          delayHide: 800,
          delayShow: 0,
          flip: true,
          mode: 'click, hover',
          offset: 0,
          pos: 'bottom-left',
        }, (this.props.dropDown ? this.props.dropDown as any : this.props.drop as any)),
      } : {},
    };
  }

  protected abstract get content(): JSX.Element | JSX.Element[];
}
