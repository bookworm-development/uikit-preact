import { computed } from 'mobx';

import { Alignment, toAlignClasses } from '../align';
import { Animation, AnimationType, toAnimationClasses } from '../animation';
import { Flex, toFlexClasses } from '../flex';
import {
  Height, IHeightMatch, IHeightViewport, toHeightClasses, toMatchAttribute, toViewportAttribute,
} from '../height';
import { Margin, toMarginClasses } from '../margin';
import { Padding, toPaddingClasses } from '../padding';
import { Position, toPositionClasses } from '../position';
import { Text, toTextClasses } from '../text';
import { toTransitionClasses, Transition } from '../transition';
import { toVisibilityAttribute, toVisibilityClasses, Visibility } from '../visibility';
import { toWidthClasses, Width } from '../width';
import { BaseComponent, IBaseComponent } from './base';

export interface IBaseElementComponent extends IBaseComponent {
  alignment?: Alignment | Alignment[];
  animation?: Animation | Animation[];
  onAnimationStart?: (ev: AnimationEvent) => void;
  onAnimationStartCapture?: (ev: AnimationEvent) => void;
  onAnimationEnd?: (ev: AnimationEvent) => void;
  onAnimationEndCapture?: (ev: AnimationEvent) => void;
  onAnimationIteration?: (ev: AnimationEvent) => void;
  onAnimationIterationCapture?: (ev: AnimationEvent) => void;
  borderRadius?: 'rounded' | 'circle' | 'pill';
  boxShadow?: BoxShadow | BoxShadow[];
  drag?: boolean;
  dropCap?: boolean;
  filterContainerIdentifier?: string;
  filters?: Array<{ attribute: string; value: string; }>;
  flex?: Flex | Flex[];
  float?: 'left' | 'right' | 'clear';
  height?: Height | IHeightViewport;
  heightMatch?: IHeightMatch;
  inline?: true | 'clip';
  inverse?: 'light' | 'dark';
  margin?: Margin | Margin[];
  overflow?: 'hidden' | 'auto';
  padding?: Padding | Padding[];
  parallax?: Parallax;
  position?: Position | Position[];
  resize?: true | 'vertical';
  responsive?: 'width' | 'height' | 'preserve';
  scrollspy?: IScrollSpy;
  sortable?: ISortable;
  sticky?: IStickyOptions;
  switchable?: boolean | string;
  switcher?: ISwitcherOptions;
  text?: Text | Text[];
  tooltip?: ITooltip;
  transform?: 'center';
  transformOrigin?:
    'top-left' | 'top-center' | 'top-right' | 'center-left' | 'center-right' |
    'bottom-left' | 'bottom-center' | 'bottom-right';
  transition?: Transition | Transition[];
  onTransitionStart?: (ev: TransitionEvent) => void;
  onTransitionRun?: (ev: TransitionEvent) => void;
  onTransitionCancel?: (ev: TransitionEvent) => void;
  onTransitionEnd?: (ev: TransitionEvent) => void;
  visibility?: Visibility;
  width?: Width | Width[];
}

interface ISortable {
  group?: string;
  animation?: number;
  threshold?: number;
  ['cls-item']: string;
  ['cls-placeholder']: string;
  ['cls-drag']: string;
  ['cls-drag-state']: string;
  ['cls-base']: string;
  ['cls-no-drag']: string;
  ['cls-empty']: string;
  ['cls-custom']: string;
  handle?: boolean | string;
}

export interface ISwitcherOptions {
  active?: number;
  animation?: AnimationType | AnimationType[];
  connect?: boolean | string;
  duration?: number;
  swiping?: boolean;
  toggle?: string;
}

interface IStickyOptions {
  ['cls-active']?: string;
  ['cls-inactive']?: string;
  ['show-on-up']?: boolean;
  ['target-offset']?: boolean | number;
  ['width-element']?: string;
  animation?: boolean | AnimationType;
  bottom?: number | string;
  media?: number | string | boolean;
  offset?: number;
  top?: number | string;
}

interface IScrollSpy {
  cls?: 'uk-scrollspy-inview' | Animation;
  hidden?: boolean;
  ['offset-top']?: number;
  ['offset-left']?: number;
  repeat?: boolean;
  delay?: number;
}

interface ITooltip {
  title: string;
  pos?:
    'top' | 'top-left' | 'top-right' |
    'bottom' | 'bottom-left' | 'bottom-right' |
    'left' | 'right';
  offset?: boolean | number;
  animation?: { in: AnimationType | AnimationType[]; out: AnimationType | AnimationType[] };
  duration?: number;
  delay?: number;
  cls?: string;
}

interface IParallax {
  easing?: number;
  media?: number | string | boolean;
  target?: boolean | string;
  viewport?: number;
}
interface ICSSParallax  { [cssValueProp: string]: number | number[]; }

type Parallax = IParallax | ICSSParallax;

type BoxShadow =
  'small' | 'medium' | 'large' | 'xlarge' |
  'bottom' |
  'hover-small' | 'hover-medium' | 'hover-large' | 'hover-xlarge';

export abstract class ElementComponent<P extends IBaseElementComponent, S> extends BaseComponent<P, S> {
  /**
   * Generates the list of class names that can be attached to an element component
   */
  protected get className() {
    return this.toClassName([
      // Base class from parent
      super.className,

      // Alignment
      toAlignClasses(this.props.alignment),

      // Animations
      toAnimationClasses(this.props.animation),

      // Border radius
      this.props.borderRadius ? `uk-border-${this.props.borderRadius}` : '',

      // Box Shadow
      toBoxShadowClasses(this.props.boxShadow),

      // Drag
      this.props.drag ? 'uk-drag' : '',

      // Drop cap
      this.props.dropCap ? 'uk-dropcap' : '',

      // Flex
      toFlexClasses(this.props.flex),

      // Filter container identifier
      this.props.filterContainerIdentifier ? this.props.filterContainerIdentifier as string : '',

      // Float
      this.props.float ?
        this.props.float === 'clear' ? 'uk-clearfix' :
          `uk-float-${this.props.float}` :
        '',

      // Height
      typeof this.props.height === 'string' ?
        toHeightClasses(this.props.height as Height) : '',

      // Inline
      this.props.inline ?
        this.props.inline === true ? 'uk-inline' : `uk-inline-${this.props.inline}` :
        '',

      // Inverse
      this.props.inverse ? `uk-${this.props.inverse}` : '',

      // Margin
      toMarginClasses(this.props.margin),

      // Overflow
      this.props.overflow ? `uk-overflow-${this.props.overflow}` : '',

      // Padding
      toPaddingClasses(this.props.padding),

      // Position
      toPositionClasses(this.props.position),

      // Text
      toTextClasses(this.props.text),

      // Resize
      this.props.resize ?
        this.props.resize === 'true' ? 'uk-resize' : `uk-resize-${this.props.resize}` :
        '',

      // Responsive
      this.props.responsive ?
        this.props.responsive === 'preserve' ? 'uk-preserve-width' : `uk-responsive-${this.props.responsive}` :
        '',

      // Switchable content (determines the class to be added to the switchable content)
      this.props.switchable ?
        typeof this.props.switchable === 'string' ?
          this.props.switchable :
          'uk-switcher' :
        '',

      // Transform
      this.props.transform ? `uk-transform-${this.props.transform}` : '',

      // Transform origin
      this.props.transformOrigin ? `uk-transform-origin-${this.props.transformOrigin}` : '',

      // Transition
      toTransitionClasses(this.props.transition),

      // Visibility
      toVisibilityClasses(this.props.visibility),

      // Width
      toWidthClasses(this.props.width),
    ]);
  }

  /**
   * Generates the attributes map associated with an element component
   */
  protected get attributes() {
    return {
      ...super.attributes,

      // Animations
      ...this.props.onAnimationStart ? { onAnimationStart: this.props.onAnimationStart } : {},
      ...this.props.onAnimationStartCapture ? { onAnimationStartCapture: this.props.onAnimationStartCapture } : {},
      ...this.props.onAnimationEnd ? { onAnimationEnd: this.props.onAnimationEnd } : {},
      ...this.props.onAnimationEndCapture ? { onAnimationEndCapture: this.props.onAnimationEndCapture } : {},
      ...this.props.onAnimationIteration ? { onAnimationIteration: this.props.onAnimationIteration } : {},
      ...this.props.onAnimationIterationCapture ?
        { onAnimationIterationCapture: this.props.onAnimationIterationCapture } : {},

      // Filterables
      ...this.filters,

      // Viewport
      ...toViewportAttribute(this.props.height as IHeightViewport),

      // Match height
      ...toMatchAttribute(this.props.heightMatch as IHeightMatch),

      // Parallax
      ...this.parallax,

      // Visibility
      ...toVisibilityAttribute(this.props.visibility),

      // Sortable
      ...this.props.sortable ?
        {
          ['uk-sortable']: this.toAttributeValue(
            {
              animation: 150,
              group: '',
              handle: false,
              threshold: 10,
              ['cls-item']: 'uk-sortable-item',
              ['cls-placeholder']: 'uk-sortable-placeholder',
              ['cls-drag']: 'uk-sortable-drag',
              ['cls-drag-state']: 'uk-sortable-dragging',
              ['cls-base']: 'uk-sortable',
              ['cls-no-drag']: 'uk-sortable-nodrag',
              ['cls-empty']: 'uk-sortable-empty',
              ['cls-custom']: '',
            },
            this.props.sortable as any,
          ),
        } : {},

      // Scroll spy
      ...this.scrollspy,

      // Sticky
      ...this.props.sticky ?
        {
          ['uk-sticky']: this.toAttributeValue(
            {
              ['cls-active']: 'uk-active',
              ['cls-inactive']: '',
              ['show-on-up']: false,
              ['target-offset']: false,
              ['width-element']: false,
              animation: false,
              bottom: false,
              media: false,
              offset: 0,
              top: 0,
            },
            this.props.sticky as IStickyOptions,
          ),
        } :
        {},

      // Switcher
      ...this.props.switcher ?
        {
          ['uk-switcher']: this.toAttributeValue(
            {
              active: 0,
              animation: false,
              connect: '~.uk-switcher',
              duration: 200,
              swiping: true,
              toggle: '> * > :first-child',
            },
            this.props.switcher as ISwitcherOptions,
          ),
        } :
        {},

      // Tooltip
      ...this.tooltip,

      // Transition
      ...this.props.onTransitionStart ? { onTransitionStart: this.props.onTransitionStart } : {},
      ...this.props.onTransitionEnd ? { onTransitionEnd: this.props.onTransitionEnd } : {},
      ...this.props.onTransitionCancel ? { onTransitionCancel: this.props.onTransitionCancel } : {},
      ...this.props.onTransitionRun ? { onTransitionRun: this.props.onTransitionRun } : {},
    };
  }

  @computed
  private get scrollspy() {
    const options: IScrollSpy = this.props.scrollspy ? { ...this.props.scrollspy as IScrollSpy } : {};

    if (options.cls !== 'uk-scrollspy-inview') {
      const cls = toAnimationClasses(options.cls) as AnimationType;

      if (cls) {
        options.cls = cls;
      }
    }

    return this.props.scrollspy ? {
      ['uk-scrollspy']: this.toAttributeValue(
        {
          ['offset-left']: 0,
          ['offset-top']: 0,
          cls: 'uk-scrollspy-inview',
          delay: 0,
          hidden: true,
          repeat: false,
        },
        options as any,
      ),
    } : {};
  }

  @computed
  private get tooltip() {
    if (this.props.tooltip) {
      const options: ITooltip = {...this.props.tooltip as ITooltip};

      if (options.animation) {
        options.animation = [
          toAnimationClasses(options.animation.in),
          toAnimationClasses(options.animation.out),
        ].filter((a) => !!a.length).join(', ') as any;
      }

      return {
        ['uk-tooltip']: this.toAttributeValue(
          {
            animation: 'uk-animation-scale-up',
            cls: 'uk-active',
            delay: 0,
            duration: 100,
            offset: false,
            pos: 'top',
            title: '',
          },
          options,
        ),
      };
    }

    return {};
  }

  @computed
  private get parallax() {
    if (this.props.parallax) {
      const parallax = this.props.parallax as Parallax;
      const defaultOptions = {
        easing: 1,
        media: false,
        target: false,
        viewport: 1,
      };

      return {
        ['uk-parallax']: [
          this.toAttributeValue(
            defaultOptions,
            parallax,
          ),
          Object.keys(parallax).
            filter((p) => !(p in defaultOptions)).
            map((p) => {
              if ((parallax as ICSSParallax)[p] instanceof Array) {
                return `${p}: ${((parallax as ICSSParallax)[p] as number[]).join(', ')}`;
              }

              return `${p}: ${(parallax as ICSSParallax)[p]}`;
            }),
        ].filter((s) => s.length).join('; '),
      };
    }

    return {};
  }

  @computed
  private get filters() {
    if (this.props.filters) {
      const filters: { [prop: string]: any; } = {};

      this.props.filters.forEach((f) => {
        filters[`data-${f.attribute}`] = f.value;
      });

      return filters;
    }

    return {};
  }
}

export function toBoxShadowClasses(
  boxShadow: BoxShadow | BoxShadow[] | undefined) {
  const b: BoxShadow[] = boxShadow ?
    typeof boxShadow === 'string' ? [boxShadow] : boxShadow : [];

  return b.
    filter((bb) => !!bb).
    map((bb) => `uk-box-shadow-${bb}`).
    join(' ');
}
