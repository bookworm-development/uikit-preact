export type MarginElement = true | 'top' | 'bottom' | 'left' | 'right';

export type MarginSizes =
  'small' | 'small-top' | 'small-bottom' | 'small-left' | 'small-right' |
  'medium' | 'medium-top' | 'medium-bottom' | 'medium-left' | 'medium-right' |
  'large' | 'large-top' | 'large-bottom' | 'large-left' | 'large-right' |
  'xlarge' | 'xlarge-top' | 'xlarge-bottom' | 'xlarge-left' | 'xlarge-right';

export type MarginRemover =
  'remove' | 'remove-top' | 'remove-bottom' | 'remove-left' | 'remove-right' | 'remove-vertical' | 'remove-adjacent';

export type MarginAuto =
  'auto' | 'auto-top' | 'auto-bottom' | 'auto-left' | 'auto-right' | 'auto-vertical';

export type Margin =
  MarginElement | MarginSizes | MarginRemover | MarginAuto;

export function toMarginClasses(
  margin: Margin | Margin[] | undefined) {
  const m: Margin[] = margin ?
    (typeof margin === 'string' || typeof margin === 'boolean') ? [margin] : margin : [];

  return m.
    filter((mm) => !!mm).
    map((mm) => `uk-margin${ mm === true ? '' : `-${mm}`}`).
    join(' ');
}
