export type PaddingElement = true | 'small' | 'large';

export type PaddingRemover =
  'remove' | 'remove-top' | 'remove-bottom' | 'remove-left' | 'remove-right' | 'remove-vertical' | 'remove-horizontal';

export type Padding =
  PaddingElement | PaddingRemover;

export function toPaddingClasses(
  padding: Padding | Padding[] | undefined) {
  const p: Padding[] = padding ?
    (typeof padding === 'string' || typeof padding === 'boolean') ? [padding as Padding] : padding : [];

  return p.
    filter((pp) => !!pp).
    map((mm) => `uk-padding${ mm === true ? '' : `-${mm}`}`).
    join(' ');
}
