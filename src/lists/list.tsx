import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IListProps extends IBaseElementComponent {
  styleType?: 'striped' | 'bullet' | 'divider';
  large?: boolean;
  items: IItem[];
}

export interface IItem {
  content: JSX.Element | JSX.Element[] | string;
}

@observer
export class List extends ElementComponent<IListProps, any> {
  protected baseClass = 'uk-list';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      { ...this.items }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Large
      this.props.large ? 'uk-list-large' : '',

      // Style
      this.props.styleType ? `uk-list-${this.props.styleType}` : '',
    ]);
  }

  @computed
  protected get items() {
    return this.props.items.map((i) => {
      return <li>{i.content}</li>;
    });
  }
}
