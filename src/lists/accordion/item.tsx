import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';

type IAccordionItemProps = IBaseElementComponent & IAccordionItem;

export interface IAccordionItem {
  content?: JSX.Element | JSX.Element[] | string;
  title?: JSX.Element | JSX.Element[] | string;

  open?: boolean;
}

@observer
export class Item extends ElementComponent<IAccordionItemProps, any> {
  private static ClassTitle = 'uk-accordion-title';
  private static ClassContent = 'uk-accordion-content';
  private static ClassOpen = 'uk-open';

  protected baseClass = '';

  public render() {
    return <li className={this.className} {...this.attributes}>
      {...this.title}
      {this.content}
    </li>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Open status
      this.props.open ? Item.ClassOpen : '',
    ]);
  }

  @computed
  protected get title() {
    const title: Array<JSX.Element | null> = typeof this.props.title === 'string' ?
      [
        <a class={Item.ClassTitle} href={'#'}>
          {this.props.title}
        </a>,
      ] :
      this.props.title instanceof Array ? this.props.title :
      this.props.title ? [this.props.title] :
      [null];

    return title;
  }

  @computed
  protected get content() {
    const content: Array<JSX.Element | null | string> =
      typeof this.props.content === 'string' ?
        [this.props.content] :
        this.props.content instanceof Array ? this.props.content :
          this.props.content ? [this.props.content] :
      [null];

    if (content[0] === null) {
      return null;
    }

    return <div className={Item.ClassContent}>
      {...content}
    </div>;
  }
}
