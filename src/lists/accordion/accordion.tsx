import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { IAccordionItem, Item } from './item';

export interface IAccordionProps extends IBaseElementComponent {
  items: IAccordionItem[];

  collapsible?: boolean;
  multiple?: boolean;
  open?: number;
  animationDuration?: number;
}

@observer
export class Accordion extends ElementComponent<IAccordionProps, any> {
  private static DefaultAttributeDefaults = {
    animationDuration: 200,
    collapsible: true,
    multiple: false,
  };

  protected baseClass = '';

  public render() {
    const toOpen = this.props.open !== undefined ? this.props.open : 0;

    return <ul className={this.className} {...this.attributes}>
      {
        this.props.items.map((i, ii) => <Item {...i} {...ii === toOpen ? {open: true} : {}} />)
      }
    </ul>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      ['uk-accordion']: this.accordionAttributes,
    };
  }

  @computed
  protected get accordionAttributes() {
    return this.toAttributeValue(Accordion.DefaultAttributeDefaults, this.props);
  }
}
