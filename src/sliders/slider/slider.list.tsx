import { computed, ElementComponent, h, IBaseElementComponent, observer, Omit, toWidthClasses } from '../../core';
import { ISliderProps } from './slider';
import { Item } from './slider.item';

@observer
export class SliderList extends ElementComponent<Omit<ISliderProps, 'options'> & IBaseElementComponent, any> {
  protected baseClass = 'uk-slider-items';

  public render() {
    return <ul className={this.className} {...this.attributes}>
      {
        this.props.items.map((i) => <Item {...i} />)
      }
    </ul>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Child widths
      this.props.childWidth ? toWidthClasses(this.props.childWidth) : '',

      // Glutter
      this.props.glutter ? 'uk-grid' : '',
    ]);
  }
}
