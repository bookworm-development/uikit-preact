import {
  ElementChild, ElementComponent, h, IBaseElementComponent, observer,
} from '../../core';
import { IImageProps, Image } from '../../images/image';

interface IItemContent {
  content: ElementChild;
}

interface IItemImage {
  image: IImageProps;
  overlay?: ElementChild;
}

export type SliderItem = IItemImage | IItemContent;

interface ISliderItemProps extends IBaseElementComponent {
  image?: IImageProps;
  overlay?: ElementChild;
  content?: ElementChild;
}

@observer
export class Item extends ElementComponent<ISliderItemProps, any> {
  protected baseClass = '';

  public render() {
    return <li {...this.attributes} className={this.className}>
        {
          this.props.content ?
            this.props.content :
            [
              this.props.image ?
                <Image {...this.props.image} /> : null,
              this.props.overlay ?
                this.props.overlay : null,
            ]
        }
      </li>;
  }
}
