import { ElementChild, ElementComponent, h, IBaseElementComponent, observer } from '../../core';
import { IImageProps, Image } from '../../images/image';
import { IVideoProps, Video } from '../../video/video';

interface IItemContent {
  content: ElementChild;
}

interface IItemImage {
  image: IImageProps;
  overlay?: ElementChild;
}

interface IItemVideo {
  video: IVideoProps;
  overlay?: ElementChild;
}

export type SlideshowItem = IItemImage | IItemContent | IItemVideo;

interface ISlideshowItemProps extends IBaseElementComponent {
  image?: IItemImage['image'];
  overlay?: IItemImage['overlay'];
  content?: IItemContent['content'];
  video?: IItemVideo['video'];
}

@observer
export class Item extends ElementComponent<ISlideshowItemProps, any> {
  protected baseClass = '';

  public render() {
    return <li {...this.attributes} className={this.className}>
        {
          this.props.content ?
            this.props.content :
            [
              this.props.image ?
                <Image {...this.props.image} /> : null,

              this.props.video ?
                <Video {...this.props.video} /> : null,

              this.props.overlay ?
                this.props.overlay : null,
            ]
        }
      </li>;
  }
}
