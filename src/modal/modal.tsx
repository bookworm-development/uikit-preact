import { computed, ElementChild, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { Body } from './modal.body';
import { Close } from './modal.close';
import { CloseButton } from './modal.close.button';

export interface IModalProps extends IBaseElementComponent {
  id: string;
  options?: IModalOptions;
  close?: boolean | 'outside';
  center?: boolean;
  header?: ElementChild;
  footer?: ElementChild;
  container?: boolean;
  full?: boolean;
}

interface IModalOptions {
  ['bg-close']: boolean;
  ['cls-page']: string;
  ['cls-panel']: string;
  ['esc-close']: boolean;
  ['sel-close']: string;
  container: boolean | string;
  stack: boolean;
}

@observer
export class Modal extends ElementComponent<IModalProps, any> {
  public static CloseButton = CloseButton;

  protected baseClass = '';

  public render() {
    const separateContent = !!this.props.header || !!this.props.footer;

    return <div className={this.className} {...this.attributes}>
      <Body
        center={this.props.center} {...separateContent ? { container: true } : { body: true, container: true }}
        overflow={this.props.overflow}>
        {
          this.props.close ?
            <Close full={this.props.full} {...this.props.close === 'outside' ? { outside: true } : {}} /> :
            null
        }
        { this.props.header ?
            <div className={'uk-modal-header'}>{this.props.header}</div> :
            null
        }
        {
          separateContent ?
            <Body body>
              {this.props.children}
            </Body> :
            this.props.children
        }
        { this.props.footer ?
            <div className={'uk-modal-footer'}>{this.props.footer}</div> :
            null
        }
      </Body>
    </div>;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Center
      this.props.center ? 'uk-flex-top' : '',

      // Container
      this.props.container ? 'uk-modal-container' : '',

      // Full
      this.props.full ? 'uk-modal-full' : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Modal id (to allow toggle)
      id: this.props.id,

      // Modal
      ['uk-modal']: this.toAttributeValue(
        {
          ['bg-close']: true,
          ['cls-page']: 'uk-modal-page',
          ['cls-panel']: 'uk-modal-dialog',
          ['esc-close']: true,
          ['sel-close']: '.uk-modal-close, .uk-modal-close-default, .uk-modal-close-outside, .uk-modal-close-full',
          container: true,
          stack: false,
        },
        this.props.options || {},
      ),
    };
  }
}
