import { Button, IButtonProps } from '../button/button';
import { computed, ElementComponent, h, observer } from '../core';

@observer
export class CloseButton extends ElementComponent<Partial<IButtonProps>, any> {
  protected baseClass = '';

  public render() {
    return <Button {...this.props as {[p: string]: any}} class={this.className} />;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Make the button close the modal
      'uk-modal-close',
    ]);
  }
}
