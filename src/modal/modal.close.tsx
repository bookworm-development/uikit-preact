import { Close as GenericClose } from '../close';
import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface IModalCloseProps extends IBaseElementComponent {
  outside?: boolean;
  full?: boolean;
}

@observer
export class Close extends ElementComponent<IModalCloseProps, any> {
  protected baseClass = '';

  public render() {
    return <GenericClose class={this.className} forceAttributes={this.attributes} />;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Full and outside/inside
      this.props.full ?
        'uk-modal-close-full' :
        this.props.outside ? 'uk-modal-close-outside' : 'uk-modal-close-default',
    ]);
  }
}
