import { Component, h, observer } from '../core';

export interface IBreadcrumbItem {
  disabled?: boolean;
  label: string;
  link?: string;
}

@observer
export class BreadcrumbItem extends Component<IBreadcrumbItem, any> {
  public render() {
    return <li className={this.props.disabled ? 'uk-disabled' : ''}>
      {
        this.props.disabled ?
          <span>{this.props.label}</span> :
          <a href={this.props.link || '#'}>{this.props.label}</a>
      }
    </li>;
  }
}
