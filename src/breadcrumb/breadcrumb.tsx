import { ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { BreadcrumbItem, IBreadcrumbItem } from './breadcrumb.item';

export interface IBreadcrumbProps extends IBaseElementComponent {
  items: IBreadcrumbItem[];
}

@observer
export class Breadcrumb extends ElementComponent<IBreadcrumbProps, any> {
  protected baseClass = 'uk-breadcrumb';

  public render() {
    const n = this.props.items.length - 1;

    return <ul className={this.className} {...this.attributes}>
      {
        ...this.props.items.map((conf, i) => {
          return <BreadcrumbItem {...conf} disabled={i === n ? true : conf.disabled} />;
        })
      }
    </ul>;
  }
}
