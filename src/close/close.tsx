import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

export interface ICloseProps extends IBaseElementComponent {
  large?: boolean;
}

@observer
export class Close extends ElementComponent<ICloseProps, any> {
  protected baseClass = '';

  public render() {
    return <button className={this.className} {...this.attributes} type={'button'} />;
  }

  @computed
  protected get className() {
    return this.toClassName([
      super.className,

      // Large
      this.props.large ? 'uk-close-large' : '',
    ]);
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Close
      ['uk-close']: true,
    };
  }
}
