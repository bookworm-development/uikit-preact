import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';

interface IOffCanvasProps extends IBaseElementComponent {
  id: string;

  ['bg-close']?: boolean;
  container?: string;
  ['esc-close']?: boolean;
  flip?: boolean;
  mode?: 'slide' | 'reveal' | 'push' | 'none';
  overlay?: boolean;
}

@observer
export class OffCanvas extends ElementComponent<IOffCanvasProps, any> {
  protected baseClass = '';

  public render() {
    return <div class={this.className} {...this.attributes}>
      {this.props.children}
    </div>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Id
      id: this.props.id,

      // OffCanvas attribute
      ['uk-offcanvas']: this.toAttributeValue(
        {
          ['bg-close']: true,
          container: true,
          ['esc-close']: true,
          flip: false,
          mode: 'slide',
          overlay: false,
        },
        this.props,
      ),
    };
  }
}
