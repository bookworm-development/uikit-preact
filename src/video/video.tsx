import { computed, ElementComponent, h, IBaseElementComponent, observer } from '../core';
import { IVideoSource, Source } from './source';

export interface IVideoProps extends IBaseElementComponent {
  autoplay?: boolean;
  controls?: boolean;
  loop?: boolean;
  muted?: boolean;
  playsinline?: boolean;
  sources: IVideoSource[];
  automute?: boolean;
}

@observer
export class Video extends ElementComponent<IVideoProps, any> {
  protected baseClass = '';

  public render() {
    return <video className={this.className} {...this.attributes}>
      { this.props.sources.map((s) => <Source {...s} />) }
    </video>;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Controls
      ...this.props.controls ? { controls: this.props.controls } : {},

      // Loop
      ...this.props.loop ? { loop: this.props.loop } : {},

      // Muted
      ...this.props.muted ? { muted: this.props.muted } : {},

      // Play inline
      ...this.props.playsinline ? { playsinline: this.props.playsinline } : {},

      // UkVideo with autoplay and automute value attributes
      ['uk-video']: this.toAttributeValue({ autoplay: true, automute: false }, this.props),
    };
  }
}
