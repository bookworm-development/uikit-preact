import { BaseComponent, computed, h, IBaseComponent, observer } from '../core';

export interface IVideoSource extends IBaseComponent {
  media?: string;
  sizes?: string;
  src: string;
  srcset?: string;
  type: string;
}

@observer
export class Source extends BaseComponent<IVideoSource, any> {
  protected baseClass = '';

  public render() {
    return <source className={this.className} {...this.attributes} />;
  }

  @computed
  protected get attributes() {
    return {
      ...super.attributes,

      // Media
      ...this.props.media ? { media: this.props.media } : {},

      // Sizes
      ...this.props.sizes ? { sizes: this.props.sizes } : {},

      // Src
      src: this.props.src,

      // Srcset
      ...this.props.srcset ? { srcset: this.props.srcset } : {},

      // Type
      type: this.props.type,
    };
  }
}
